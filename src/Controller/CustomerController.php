<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Repository\CustomerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Exception;

/**
 * Class CustomerController
 * @package App\Controller
 * @Route("/api", name="customer_api")
 */
class CustomerController extends AbstractController
{
    /**
     * @param CustomerRepository $customerRepository
     * @return JsonResponse
     * @Route("/customers", name="customers", methods={"GET"})
     */
    public function getCustomers(CustomerRepository $customerRepository): JsonResponse
    {
        $data = $customerRepository->findAll();
        return $this->response($data);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     * @throws Exception
     * @Route("/customer", name="customer_create", methods={"POST"})
     */
    public function createCustomer(
        Request                $request,
        EntityManagerInterface $entityManager,
    ): JsonResponse {
        try {
            $request = $this->transformJsonBody($request);
            $this->checkRequest($request);

            $customer = new Customer();
            $customer->setName($request->get('name'));
            $customer->setFirstname($request->get('firstname'));
            $customer->setLastname($request->get('lastname'));
            $customer->setPhone($request->get('phone'));
            $customer->setEmail($request->get('email'));

            $entityManager->persist($customer);
            $entityManager->flush();

            return $this->response(['customerId' => $customer->getId()], 201);

        } catch (Exception $e) {
            $data = [
                'status' => $e->getCode(),
                'errors' => $e->getMessage(),
            ];
            return $this->response($data, $e->getCode());
        }
    }

    /**
     * @param CustomerRepository $customerRepository
     * @param $id
     * @return JsonResponse
     * @Route("/customer/{id}", name="customer_get", methods={"GET"})
     */
    public function getCustomer(CustomerRepository $customerRepository, $id): JsonResponse
    {
        $customer = $customerRepository->find($id);

        if (!$customer) {
            $data = [
                'status' => 404,
                'errors' => "Customer not found",
            ];
            return $this->response($data, 404);
        }
        return $this->response($customer);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param CustomerRepository $customerRepository
     * @param string $id
     * @return JsonResponse
     * @Route("/customer/{id}", name="customer_put", methods={"PUT"})
     */
    public function updateCustomer(
        Request                 $request,
        EntityManagerInterface  $entityManager,
        CustomerRepository      $customerRepository,
        string                  $id
    ): JsonResponse {
        try {
            $customer = $customerRepository->find($id);

            if (!$customer) {
                $data = [
                    'status' => 404,
                    'errors' => "Customer not found",
                ];
                return $this->response($data, 404);
            }

            $request = $this->transformJsonBody($request);
            $this->checkRequest($request);

            $customer->setName($request->get('name'));
            $customer->setFirstname($request->get('firstname'));
            $customer->setLastname($request->get('lastname'));
            $customer->setPhone($request->get('phone'));
            $customer->setEmail($request->get('email'));

            $entityManager->flush();

            return $this->response($customer);

        } catch (\Exception $e) {
            $data = [
                'status' => $e->getCode(),
                'errors' => $e->getMessage(),
            ];
            return $this->response($data, 422);
        }
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param CustomerRepository $customerRepository
     * @param string $id
     * @Route("/customer/{id}", name="customer_delete", methods={"DELETE"})
     * @return JsonResponse
     */
    public function deleteCustomer(
        EntityManagerInterface  $entityManager,
        CustomerRepository      $customerRepository,
        string                  $id
    ): JsonResponse
    {
        $customer = $customerRepository->find($id);

        if (!$customer) {
            $data = [
                'status' => 404,
                'errors' => "Customer not found",
            ];
            return $this->response($data, 404);
        }

        $entityManager->remove($customer);
        $entityManager->flush();
        $data = [
            'status' => 200,
            'errors' => "Customer deleted successfully",
        ];
        return $this->response($data);
    }

    /**
     * Returns a JSON response
     *
     * @param array| Customer|null $data
     * @param int $status
     * @param array $headers
     * @return JsonResponse
     */
    public function response(Customer|array $data = null, int $status = 200, array $headers = []): JsonResponse
    {
        return new JsonResponse($data, $status, $headers);
    }

    protected function transformJsonBody(Request $request): Request
    {
        $data = json_decode($request->getContent(), true);

        if ($data === null) {
            return $request;
        }

        $request->request->replace($data);

        return $request;
    }

    private function checkRequest(Request $request)
    {
        if (
            !$request->get('name')
            || !$request->get('firstname')
            || !$request->get('lastname')
            || !$request->get('phone')
            || !$request->get('email')
        ) {
            throw new \Exception(
                'name, firstname, lastname, phone and email is required',
                400
            );
        }
    }
}
